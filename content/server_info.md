+++
title = "New River Valley Rocks"
date = "2021-02-01"
+++


## Discord 

Join the conversation!

- [Discord Online Jamming Central](https://discord.gg/wV2fMfC442)

- [New River Valley Rocks](https://discord.gg/gK2TtaEEw2)

## Ninjam

[Ninjam]((https://www.cockos.com/ninjam/)) is freely available open source software created by the developers of [Reaper](https://www.reaper.fm/), a popular Digital Audio Workstation.

- [Public Ninjam servers](ttp://autosong.ninjam.com/server-list.php)
  
- ~~nrv.rocks:2049~~ Local servers are online by request, due to a lack of recent activity.

## Icecast

Ninbot streams all public servers, live.

  - https://ninbot.com/

